﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MapGenerator : MonoBehaviour
{
    private const float CrystalRandom = 0.2f;
    private const float TileRandom = 0.4f;
    
    private const int TilesOnStart = 15;
    
    private const string Root = "Prefabs/";
    private const string RootPlayer = Root + "Player";
    private const string RootTile = Root + "Tile";
    private const string RootStartTile = Root + "Start";
    private const string RootCrystal = Root + "Crystal";
    
    [SerializeField] private GameObject container;

    private Vector3 _lastPosition;
    private GameObject _tile;
    private GameObject _crystal;

    private void Start()
    {
        _crystal = (GameObject)Resources.Load(RootCrystal);
        _tile = (GameObject)Resources.Load(RootTile);
        GenerateStartTile();
        Events.OnTileDestroy += GenerateTile;
    }
    
    private void OnDestroy()
    {
        Events.OnTileDestroy -= GenerateTile;
    }

    public static void Restart()
    {
        SceneManager.LoadScene(0);
    }

    private void GenerateStartTile()
    {
        Instantiate(Resources.Load(RootStartTile), container.transform);
        GameObject tile = Instantiate(_tile, container.transform);
        _lastPosition = tile.transform.position = new Vector3(0, 0, 6);
        GeneratePlayer();
        for (int i = 0; i < TilesOnStart; i++)
        {
            GenerateTile();
        }
    }

    private void GenerateTile()
    {
        GameObject tile = Instantiate(_tile, container.transform);
        tile.transform.position = Random.Range(0f, 1f) <= TileRandom ? new Vector3(_lastPosition.x + 2f, _lastPosition.y, _lastPosition.z) : new Vector3(_lastPosition.x, _lastPosition.y, _lastPosition.z + 2f);
        if (Random.Range(0f, 1f) <= CrystalRandom)
            GenerateCrystal(tile);
        _lastPosition = tile.transform.position;
    }

    private void GenerateCrystal(GameObject tile)
    {
        Instantiate(_crystal, tile.transform);
    }
    
    private void GeneratePlayer()
    {
        Instantiate(Resources.Load(RootPlayer));
    }
}
