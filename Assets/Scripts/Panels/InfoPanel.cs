﻿using UnityEngine;
using UnityEngine.UI;

public class InfoPanel : BasePanel
{
    public override bool AlwaysActive => true;
    public override PanelType Type() => PanelType.Info;
    
    public override void ActionOnClick() { }
    
    [SerializeField] private Text scoreValue;

    private void Start()
    {
        Events.OnScoreChange += UpdateInfo;
    }
    
    private void OnDestroy()
    {
        Events.OnScoreChange -= UpdateInfo;
    }

    private void UpdateInfo()
    {
        scoreValue.text = $"Score: {Currency.Score}";
    }
}
