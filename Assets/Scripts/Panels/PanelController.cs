﻿using UnityEngine;

public class PanelController : MonoBehaviour
{
    [SerializeField] private BasePanel[] panels;

    public static PanelController Instance;

    private void Start()
    {
        Instance = this;
        ActivePanel(PanelType.Start);
    }

    public void ActivePanel(PanelType activePanel)
    {
        foreach (BasePanel panel in panels)
        {
            panel.gameObject.SetActive(panel.AlwaysActive || panel.Type() == activePanel);
        }
    }
}
