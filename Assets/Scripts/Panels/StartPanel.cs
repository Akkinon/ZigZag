﻿
public class StartPanel : BasePanel
{
    public override bool AlwaysActive => false;
    public override PanelType Type() => PanelType.Start;
    
    public override void ActionOnClick()
    {
        PanelController.Instance.ActivePanel(PanelType.Movement);
        PlayerController.StartMoving();
    }
}
