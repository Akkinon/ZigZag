﻿using UnityEngine;

public abstract class BasePanel : MonoBehaviour
{
    public abstract bool AlwaysActive { get; }
    public abstract PanelType Type();

    public abstract void ActionOnClick();
}

public enum PanelType
{
    Start,
    Death,
    Movement,
    Info
}
