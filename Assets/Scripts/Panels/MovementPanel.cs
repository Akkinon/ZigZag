﻿
public class MovementPanel : BasePanel
{
    public override bool AlwaysActive => false;
    public override PanelType Type() => PanelType.Movement;
    
    public override void ActionOnClick()
    {
        PlayerController.ChangeRotation();
    }
}
