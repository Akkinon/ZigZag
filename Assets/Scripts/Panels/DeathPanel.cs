﻿using UnityEngine;

public class DeathPanel : BasePanel
{
    public override bool AlwaysActive => false;
    public override PanelType Type() => PanelType.Death;

    public override void ActionOnClick()
    {
        Time.timeScale = 1;
        PlayerController.StopMoving();
        MapGenerator.Restart();
    }
}
