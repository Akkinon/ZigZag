﻿
public static class Events
{
    public delegate void SimpleEvents();
    
    public static event SimpleEvents OnScoreChange = delegate { };
    public static event SimpleEvents OnTileDestroy = delegate { };
    
    public static void ScoreChange()
    {
        OnScoreChange.Invoke();
    }

    public static void DestroyTile()
    {
        OnTileDestroy.Invoke();
    }
}
