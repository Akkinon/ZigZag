﻿
public static class Currency
{
    private static int score;

    public static int Score
    {
        get => score;
        private set
        {
            score = value;
            Events.ScoreChange();
        } 
    }
    public static void IncreaseScore(int value)
    {
        Score += value;
    }
}
