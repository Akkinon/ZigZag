﻿using System.Collections;
using UnityEngine;

public class CrystalBehavior : MonoBehaviour
{
    private const int Bonus = 1;
    private const float Rotate = 3;
    
    [SerializeField] private GameObject parent;
    
    private Animator _animator;

    private void Start()
    {
        _animator = GetComponent<Animator>();
    }

    void Update()
    {
        transform.Rotate(Vector3.up, Rotate);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Currency.IncreaseScore(Bonus);
            StartCoroutine(StartDestroy());
        }
    }

    IEnumerator StartDestroy()
    {
        _animator.SetTrigger("Destroy");
        yield return new WaitForSeconds(_animator.GetCurrentAnimatorStateInfo(0).length);
        Destroy(parent);
    }
}
