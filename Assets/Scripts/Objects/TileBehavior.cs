﻿using System.Collections;
using UnityEngine;

public class TileBehavior : MonoBehaviour
{
    [SerializeField] private GameObject parent;
    
    private Animator _animator;

    private void Start()
    {
        _animator = GetComponent<Animator>();
    }

    private void OnCollisionExit(Collision other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            StartCoroutine(StartDestroy());
        }
    }

    IEnumerator StartDestroy()
    {
        Events.DestroyTile();
        _animator.SetTrigger("Destroy");
        yield return new WaitForSeconds(_animator.GetCurrentAnimatorStateInfo(0).length);
        Destroy(parent);
    }
}
