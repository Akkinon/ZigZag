﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private const int Speed = 2;
    private static bool moving;
    private static bool moveForward;

    public static void StartMoving()
    {
        moving = moveForward = true;
    }
    
    public static void StopMoving()
    {
        moving = moveForward = false;
    }
    
    public static void ChangeRotation()
    {
        moveForward = !moveForward;
    }

    private void FixedUpdate()
    {
        if (!moving) return;
        gameObject.transform.Translate(moveForward ? new Vector3(0f, 0f, Speed) * Time.deltaTime : new Vector3(Speed, 0f, 0f) * Time.deltaTime);
        gameObject.transform.Translate(moveForward ? new Vector3(0f, 0f, Speed) * Time.deltaTime : new Vector3(Speed, 0f, 0f) * Time.deltaTime);

        if (gameObject.transform.position.y <= -4f)
        {
            PanelController.Instance.ActivePanel(PanelType.Death);
            Time.timeScale = 0;
        }
    }
}
